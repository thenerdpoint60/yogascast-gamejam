﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIHandler : MonoBehaviour
{

    public Slider HealthBar;

    Player player;

    private void Start()
    {
        player = FindObjectOfType<Player>();
    }


    // Update is called once per frame
    void Update()
    {
        HealthBar.value = player.health;
    }
}
