﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySmall : Enemy
{
    public float radiusOfView;
    void Start()
    {
        StartEnemy();
        isAbleToGetHeart = false;
    }

    void Update()
    {
        UpdateEnemy();
        if (IsDead())
            return;
        Jump();
    }
    private void Jump()
    {
        Transform target = GetTarget();
        bool isDistanceBetweenTarget = Vector2.Distance(transform.position, playerTransform.position) <= radiusOfView;
        if (isDistanceBetweenTarget)
            JumpTowards(target.position);
    }
}
