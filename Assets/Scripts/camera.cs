﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    public Transform playerTransform;
    public float speed;
    void Start()
    {
        speed = 15f;
    }
    void Update()
    {
        Camera cam = Camera.main;
        float height = 2f * cam.orthographicSize;
        float width = height * cam.aspect;
        float playerX = playerTransform.position.x;
        float playerY = playerTransform.position.y;
        int cameraXTimes = (int)((playerX + width / 2) / width);
        int cameraYTimes = (int)((playerY + height / 2) / height);
        if (playerX < 0)
            cameraXTimes = (int)((playerX - width / 2) / width);
        if (playerY < 0)
            cameraYTimes = (int)((playerY - height / 2) / height);
        Vector3 destination = new Vector3(cameraXTimes * width, cameraYTimes * height, -1);
        Camera.main.transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * speed);
    }
}
