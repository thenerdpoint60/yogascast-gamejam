﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimBulletProjectile : MonoBehaviour
{
    private Rigidbody2D rigidbody;
    public float duration; // duration of the bullet
    public float speed;

    private Transform player;
    private Vector2 target = Vector2.negativeInfinity;
    private int damage;
    private bool damageEnemies;
    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        damage = 10;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        //transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if (target != Vector2.negativeInfinity)
        {
            rigidbody.velocity = target.normalized * speed;
            // rotation
            Vector3 dir = target * 1000f;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        duration -= 0.1f;
        if (duration <= 0)
        {
            DestroyTheBullett();
        }
    }

    private void DestroyTheBullett()
    {
        Destroy(gameObject);
    }

    public void SetTarget(Transform target) { this.target = target.position; }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Wall"))
            DestroyTheBullett();
        if (!damageEnemies)
        {
            if (collision.CompareTag("Player"))
            {
                Player player = FindObjectOfType<Player>();
                player.Damage(damage);
                if (player.IsCanBeDamaged())
                    DestroyTheBullett();
            }
            else if (collision.CompareTag(Enemy.FRIENDLY_TAG))
            {
                Enemy enemy = collision.gameObject.GetComponent<Enemy>();
                enemy.Damage(damage);
                if (enemy.IsCanBeDamaged())
                    DestroyTheBullett();
            }
        }
        else
        {
            if (collision.CompareTag(Enemy.ENEMY_TAG))
            {
                Enemy enemy = collision.gameObject.GetComponent<Enemy>();
                enemy.Damage(damage);
                if (enemy.IsCanBeDamaged())
                    DestroyTheBullett();
            }
        }
    }
    public void SetToAllyBullet() { damageEnemies = true; }
}

