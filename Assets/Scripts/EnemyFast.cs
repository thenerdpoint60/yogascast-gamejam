﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFast : Enemy
{
    void Start()
    {
        StartEnemy();
    }

    void Update()
    {
        UpdateEnemy();
        if (IsDead())
            return;
        Jump();
    }

    private void Jump()
    {
        Transform target = GetTarget();
        float distanceBetweenPlayer = Vector2.Distance(transform.position, playerTransform.position);
        bool isTooFarAwayFromPlayer = distanceBetweenPlayer > dangerDistance;
        bool isEnemiesFarAwayFromPlayer = !IsEnemiesNearPlayer();
        if (isAlly)
        {
            if (isTooFarAwayFromPlayer)
                JumpTowards(playerTransform.position);
            else if (!isEnemiesFarAwayFromPlayer)
                JumpTowards(target.position);
            else if (GetMaximumPlayerFollowDistance() > distanceBetweenPlayer)
                JumpTowards(playerTransform.position);
        }
        else if (GetMaximumPlayerFollowDistance() < distanceBetweenPlayer)
            JumpTowards(target.position);
    }
}
