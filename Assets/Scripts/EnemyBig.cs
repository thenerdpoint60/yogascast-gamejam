﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBig : Enemy
{
    void Start()
    {
        StartEnemy();
    }
    void Update()
    {
        UpdateEnemy();
        if (IsDead())
            return;
        Move();
    }

    private void Move()
    {
        Transform target = GetTarget();
        float distanceBetweenPlayer = Vector2.Distance(transform.position, playerTransform.position);
        bool isTooFarAwayFromPlayer = distanceBetweenPlayer > dangerDistance;
        bool isEnemiesFarAwayFromPlayer = !IsEnemiesNearPlayer();
        if (isAlly)
        {
            if (isTooFarAwayFromPlayer && GetMaximumPlayerFollowDistance() < distanceBetweenPlayer)
                MoveTowards(playerTransform.position);
            else if (!isEnemiesFarAwayFromPlayer)
                MoveTowards(target.position);
        }
        else if (GetMaximumPlayerFollowDistance() < distanceBetweenPlayer)
            MoveTowards(target.position);
    }
}
