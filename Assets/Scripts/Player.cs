﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int health;
    public float speed;
    Rigidbody2D rigidbody;

    [SerializeField]
    GameObject heartObject;
    SpriteRenderer playerRenderer;
    Heart heart;
    public float heartThrowForce;

    public bool isJumpBtnDown;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        heart = heartObject.GetComponent<Heart>();
        playerRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        isJumpBtnDown = Input.GetButton("Jump");
        if (health > 0)
            MoveUpdate();
        if (Input.GetButtonDown("Jump") && heart.IsAttachedToBody() && heart.IsAttachedToBody(this.gameObject))
            heart.ThrowHeart();
        UpdateAnimation();
    }
    private float damageTime = 0; // sets the time the player will not take more damage after got damaged
    private float damageAnimTime = 0; // span between player damaged animation
    private bool damageVis = false; // damage animation when the player is visible and invisible
    private int damage; // damage taken
    private float damageTimeMax = 1f;
    private void UpdateAnimation()
    {
        // damaged animation
        if (damage > 0 && damageTime <= 0)
        {
            health -= damage;
            damageTime = damageTimeMax;
            damage = 0;
        }
        else if (damageTime > 0)
            damageTime -= Time.deltaTime;
        if (damageTime > 0)
        {
            if (damageAnimTime <= 0)
            {
                damageVis = !damageVis;
                playerRenderer.enabled = !damageVis;
                damageAnimTime = 0.1f;
            }
            else
                damageAnimTime -= Time.deltaTime;
        }
        else
        {
            damageVis = false;
            playerRenderer.enabled = true;
        }
    }
    private Vector2 vLastMovDirection; // last movement direction
    public void MoveUpdate()
    {
        // 8 direction movement
        float horizontal = Mathf.Ceil(Input.GetAxis("Horizontal") * 10f) / 10f;
        float vertical = Mathf.Ceil(Input.GetAxis("Vertical") * 10f) / 10f;
        rigidbody.velocity = new Vector2(horizontal * speed, vertical * speed);
        if (rigidbody.velocity != Vector2.zero)
            vLastMovDirection = rigidbody.velocity;
    }
    public void Damage(int damage)
    {
        if (IsCanBeDamaged())
            this.damage = damage;
    }

    public bool IsCanBeDamaged()
    {
        // can the player get damage, or is the player damage animation is still going?
        return damageTime <= 0;
    }
    public Vector2 GetMovDirection() { return vLastMovDirection; }
}
