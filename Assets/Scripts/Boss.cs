﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour //Enemy
{
    //private Animator anim;

    //public float timeBetweenShots;
    //public float startTimeBEtweenShots;
    //public float waitTimeAfterShoot;
    //public GameObject bulletProjectile;

    //// Start is called before the first frame update
    //void Start()
    //{
    //    anim = GetComponent<Animator>();
    //    timeBetweenShots = startTimeBEtweenShots;
    //}

    //// Update is called once per frame
    //void Update()
    //{

    //}

    //public void ShootSlime()
    //{


    //    if (timeBetweenShots <= 0)
    //    {
    //        anim.SetBool("isSpit", true);
    //        GameObject bulletObject = Instantiate(bulletProjectile, transform.position, Quaternion.identity);
    //        SlimBulletProjectile bulletScript = bulletObject.GetComponent<SlimBulletProjectile>();
    //        bulletScript.SetTarget(targetTransform);
    //        if (isAlly)
    //            bulletScript.SetToAllyBullet();
    //        timeBetweenShots = startTimeBEtweenShots;
    //    }
    //    else
    //    {
    //        timeBetweenShots -= Time.deltaTime;
    //        if (timeBetweenShots < 0)
    //            timeBetweenShots = 0;
    //    }
    //}
    [SerializeField]
    int numberOfProjectiles;

    [SerializeField]
    GameObject projectile;
    public float timeBetweenShots;
    public float startTimeBEtweenShots;
    Vector2 startPoint;

    float radius, moveSpeed;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        radius = 5f;
        moveSpeed = 5f;

        Vector3 pos = this.transform.position;
        startPoint = pos;
        startPoint.y = pos.y;
        anim = GetComponent<Animator>();
        timeBetweenShots = startTimeBEtweenShots;


    }

    // Update is called once per frame
    void Update()
    {
       
        SpawnProjectiles(numberOfProjectiles);
        
    }

    void SpawnProjectiles(int numberOfProjectiles)
    {
        float angleStep = 360f / numberOfProjectiles;
        float angle = 0f;

        if (timeBetweenShots <= 0)
        {
            anim.SetBool("isSpit", true);
            for (int i = 0; i <= numberOfProjectiles - 1; i++)
            {

                float projectileDirXposition = startPoint.x + Mathf.Sin((angle * Mathf.PI) / 180) * radius;
                float projectileDirYposition = startPoint.y + Mathf.Cos((angle * Mathf.PI) / 180) * radius;

                Vector2 projectileVector = new Vector2(projectileDirXposition, projectileDirYposition);
                Vector2 projectileMoveDirection = (projectileVector - startPoint).normalized * moveSpeed;

                var proj = Instantiate(projectile, startPoint, Quaternion.identity);
                proj.GetComponent<Rigidbody2D>().velocity =
                    new Vector2(projectileMoveDirection.x, projectileMoveDirection.y);

                angle += angleStep;

            }
            timeBetweenShots = startTimeBEtweenShots;
            if (numberOfProjectiles < 8)
            {
                numberOfProjectiles += 1;
            }
            else
            {
                numberOfProjectiles -= 1;
            }
        }
        else
            {
                timeBetweenShots -= Time.deltaTime;
                if (timeBetweenShots < 0)
                    timeBetweenShots = 0;
            anim.SetBool("isSpit", false);
            }

    }
}
