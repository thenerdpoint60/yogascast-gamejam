﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public const string FRIENDLY_TAG = "Friendly";
    public const string ENEMY_TAG = "Enemy";

    public Transform enemiesFolder;
    protected Rigidbody2D rigidbody;
    protected SpriteRenderer spriteRenderer;
    protected Transform targetTransform; // the target that the enemy is currently focusing on
    public float dangerDistance; // minimum distance between the enemy and the player for the ally to attack the enemy
    public int health;
    public int damageAttack; // amount of damage the enemy attacks
    public float stopFollowPlayerDistance; // stop follow player distance

    protected bool isAbleToGetHeart; // is the enemy able to collect the heart
    protected bool isAlly; // is the enemy ally to player
    public float allyDistance; // the distance the ally should always be around near the player even when attacking other enemies.
    protected bool isTargetChangable; // does the enemy focuses on only one enemy or does he searching

    public float speed;
    public float fJumpSpeed;
    public float fJumpTimerDec;
    public Transform playerTransform;
    public GameObject healthSlider;

    public GameObject heartObject;
    protected Heart heart;
    protected void StartEnemy()
    {
        isAbleToGetHeart = true;
        isAlly = false;
        targetTransform = playerTransform;
        rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        heart = heartObject.GetComponent<Heart>();
        SetTarget(playerTransform);
    }
    protected void UpdateEnemy()
    {
        if (IsDead())
        {
            if (heart != null && heart.IsAttachedToBody() && heart.IsAttachedToBody(this.gameObject))
                heart.DropHeart();
            spriteRenderer.enabled = false;
            return;
        }
        if (bIsJumping)
            JumpingTowards();
        if (isAlly)
            targetTransform = GetEnemyNearPlayer();
        else
        {
            GameObject ally = GameObject.FindGameObjectWithTag(FRIENDLY_TAG);
            if (ally == null)
                targetTransform = playerTransform;
            else if (isTargetChangable)
            {
                // pick either the player or the ally based on which one is nearest
                float playerDistance = Vector2.Distance(transform.position, playerTransform.position);
                float allyDistance = Vector2.Distance(transform.position, ally.transform.position);
                if (playerDistance < allyDistance)
                    targetTransform = playerTransform;
                else
                    targetTransform = ally.transform;
            }
        }
        UpdateAnimation();
    } 


    /* Moving functions */
    protected void MoveTowards(Vector2 targetPos)
    {
        // move towards position
        Vector2 vDirection = targetPos - (Vector2)transform.position;
        vDirection.Normalize();
        rigidbody.velocity = new Vector2(vDirection.x * speed, vDirection.y * speed);
    }
    private bool bIsJumping;
    private Vector2 vJumpDir;
    float fJumpSpeedCount;
    public float fJumpSpeedDec;
    public float fJumpTimer;
    float fJumpTimerCount;
    public float maxJumpDistance;
    Vector2 destination;
    protected void JumpTowards(Vector2 targetPos)
    {
        // jump towards position
        if (!IsFinisehdJumping())
            return;
        bIsJumping = true;
        fJumpTimerCount = fJumpTimer;
        destination = (targetPos - (Vector2)transform.position).normalized;
        fJumpSpeedCount = fJumpSpeed;
    }
    private void JumpingTowards()
    {
        // continue to jump until stop
        if (fJumpSpeedCount <= 0)
        {
            Debug.Log(fJumpSpeedCount);
            fJumpSpeedCount = 0;
            fJumpTimerCount -= fJumpTimerDec;
            if (fJumpTimerCount <= 0)
            {
                bIsJumping = false;
                fJumpTimerCount = 0;
            }
            return;
        }
        // jump
        Vector2 vThrowDir = new Vector2(destination.x * fJumpSpeedCount, destination.y * fJumpSpeedCount);
        rigidbody.velocity = vThrowDir;
        fJumpSpeedCount -= fJumpSpeedDec;
        fJumpTimerCount -= fJumpTimerDec;
    }
    private bool IsFinisehdJumping() { return fJumpTimerCount <= 0; }
    public bool IsNoLongerJumping() { return fJumpSpeedCount <= 0; }

    
    /* Checking information about player and enemies */
    public bool IsInAllyZone()
    {
        float distance = Vector2.Distance(playerTransform.position, transform.position);
        bool isInAllyZone = distance <= allyDistance;
        return isInAllyZone;
    }
    public bool IsEnemiesNearPlayer()
    {
        // check wether the player is in danger by a close enemy
        Transform nearestEnemy = GetEnemyNearPlayer();
        if (nearestEnemy == null)
            return false;
        float distance = Vector2.Distance(playerTransform.position, nearestEnemy.position);
        bool isInDangerZone = distance < dangerDistance;
        return isInDangerZone;
    }
    public Transform GetEnemyNearPlayer()
    {
        // get nearest enemy
        if (enemiesFolder.childCount < 1)
            return null;
        Transform nearest = enemiesFolder.GetChild(0);
        float distance = Vector2.Distance(playerTransform.position, enemiesFolder.GetChild(0).position);
        foreach (Transform child in enemiesFolder)
        {
            float newDis = Vector2.Distance(playerTransform.position, enemiesFolder.GetChild(0).position);
            if (newDis > distance)
            {
                nearest = child;
                distance = newDis;
            }
        }
        return nearest;
    }


    private float damageTime = 0; // sets the time the enemy will not take more damage after got damaged
    private float damageAnimTime = 0; // span between enemy damaged animation
    private bool damageVis = false; // damage animation when the enemy is visible and invisible
    private int damage; // damage taken
    private float damageTimeMax = 1f;
    private void UpdateAnimation()
    {
        // damaged animation
        if (damage > 0 && damageTime <= 0)
        {
            health -= damage;
            damageTime = damageTimeMax;
            damage = 0;
        }
        else if (damageTime > 0)
            damageTime -= Time.deltaTime;
        if (damageTime > 0)
        {
            if (damageAnimTime <= 0)
            {
                damageVis = !damageVis;
                spriteRenderer.enabled = !damageVis;
                damageAnimTime = 0.1f;
            }
            else
                damageAnimTime -= Time.deltaTime;
        }
        else
        {
            damageVis = false;
            spriteRenderer.enabled = true;
        }
    }
    public void Damage(int damage)
    {
        if (IsCanBeDamaged())
            this.damage = damage;
    }
    public bool IsCanBeDamaged()
    {
        // can the enemy get damage, or is the player damage animation is still going?
        return damageTime <= 0;
    }
    public void Die()
    {
        rigidbody.simulated = false;
    }
    public bool IsDead() { return health <= 0; }
    public float GetMaximumPlayerFollowDistance() { return stopFollowPlayerDistance; }

    private void OnTriggerStay2D(Collider2D col)
    {
        string tag = col.gameObject.tag;
        if (tag == "Heart")
        {
            if (heart != null && !heart.IsAttachedToBody())
            {
                heart.AttachToNewBody(gameObject);
                SwitchAlly(true);
            }
        }
        else if (tag == "Player")
        {
            Player player = FindObjectOfType<Player>();
            if (this as EnemySlime == null && !isAlly)
            {
                bool isDamage = true;
                if (fJumpSpeed > 0)
                    isDamage = !IsNoLongerJumping();
                if (isDamage)
                    player.Damage(damageAttack);
            }
            if (player.isJumpBtnDown)
                if (heart != null && heart.IsAttachedToBody() && heart.IsAttachedToBody(gameObject))
                {
                    heart.AttachToNewBody(col.transform.gameObject);
                    SwitchAlly(false);
                }
        }
        else if (tag == ENEMY_TAG && isAlly)
        {
            Enemy enemy = col.gameObject.GetComponent<Enemy>();
            if (this as EnemySlime == null)
            { // exception enemy slime
                bool isDamage = true;
                if (fJumpSpeed > 0)
                    isDamage = !IsNoLongerJumping();
                if (isDamage)
                    enemy.Damage(damageAttack);
            }
        }
        else if (tag == FRIENDLY_TAG && !isAlly)
        {
            Enemy enemy = col.gameObject.GetComponent<Enemy>();
            if (enemy as EnemySlime == null)
            { // exception enemy slime
                bool isDamage = true;
                if (enemy.fJumpSpeed > 0)
                    isDamage = !enemy.IsNoLongerJumping();
                if (isDamage)
                    Damage(enemy.damageAttack);
            }
        }
    }

    public void SwitchAlly(bool isAlly)
    {
        if (isAbleToGetHeart)
            this.isAlly = isAlly;
        if (isAlly)
            transform.gameObject.tag = FRIENDLY_TAG;
        else
            transform.gameObject.tag = ENEMY_TAG;
    }
    protected Vector2 RandomVector2(float angle, float angleMin)
    {
        // trying to generate random vector so the enemies would not come to the same spot each time
        float random = Random.value * angle + angleMin;
        return new Vector2(Mathf.Cos(random), Mathf.Sin(random));
    }
    public void SetTarget(Transform transform) { targetTransform = transform; }
    public Transform GetTarget() { return targetTransform; }
}
