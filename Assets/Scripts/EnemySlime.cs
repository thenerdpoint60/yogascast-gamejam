﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlime : Enemy
{
    public float stoppingDistance;
    public float retraetDistance;
    public float timeBetweenShots;
    public float startTimeBEtweenShots;
    public float waitTimeAfterShoot;
    public GameObject bulletProjectile;
    public float speedSlowDown; // the slime will slow down when it shoots
    private float speedOriginal; // saves the original speed of the slime
    private bool isStoppedToShoot; // is the enemy stops moving to shoot
    private Animator animator;
    void Start()
    {
        StartEnemy();
        timeBetweenShots = startTimeBEtweenShots;
        speedOriginal = speed;
        animator = GetComponent<Animator>();
    }
    void Update()
    {
        UpdateEnemy();
        if (IsDead())
            return;
        ShootSlime();
        if (timeBetweenShots <= startTimeBEtweenShots - waitTimeAfterShoot)
            MoveSlime();
    }
    private Vector2 randomVecAngle = Vector2.zero;

    // TODO: improve the slime
    public void MoveSlime()
    {
        // Slime movement
        // Slime moves towards the target, moves away from it and then shoots it while standing still.
        Transform target = GetTarget();
        if (randomVecAngle == Vector2.zero)
            randomVecAngle = RandomVector2(-145, 145);
        bool isTargetFarAway = Vector2.Distance(transform.position, target.position) > stoppingDistance;
        bool isTargetTooClose = Vector2.Distance(transform.position, target.position) > retraetDistance;
        bool isTooFarAwayFromPlayer = Vector2.Distance(transform.position, playerTransform.position) > dangerDistance;
        bool isEnemiesFarAwayFromPlayer = !IsEnemiesNearPlayer();
        if (isAlly && (isTooFarAwayFromPlayer || isEnemiesFarAwayFromPlayer))
        {
            // following player
            speed = speedOriginal;
            isTargetChangable = false;
            isStoppedToShoot = false;
            Vector2 vDirection = (Vector2)playerTransform.position - randomVecAngle * speed;
            MoveTowards(vDirection);
        }
        if (isTargetFarAway)
        {
            // following target
            speed = speedOriginal;
            isTargetChangable = true;
            isStoppedToShoot = false;
            Vector2 vDirection = (Vector2)target.position - randomVecAngle * speed;
            MoveTowards(vDirection);
        }
        else if (isTargetTooClose)
        {
            speed = speedOriginal;
            isTargetChangable = false;
            isStoppedToShoot = false;
            Vector2 vDirection = (Vector2)target.position - randomVecAngle * speed;
            MoveTowards(vDirection);
        }
        else
        {
            // to make the enemy retreat if the player is too close
            // or it can slide which looks cool
            speed = speedSlowDown;
            isTargetChangable = false;
            isStoppedToShoot = true;
            Vector2 vDirection = -(Vector2)Camera.main.transform.position + (Vector2)target.position - randomVecAngle * speed;
            MoveTowards(vDirection);
        }
    }

    public void ShootSlime()
    {
        // shoot and wait
        bool isShoot;
        if (isAlly)
            isShoot = IsEnemiesNearPlayer() && IsInAllyZone();
        else
            isShoot = Vector2.Distance(playerTransform.position, transform.position) < stoppingDistance;
        isShoot = isShoot && isStoppedToShoot;
        if (timeBetweenShots <= 0 && isShoot)
        {
            animator.Play("slimeShoots", 0, 0.24f);
            GameObject bulletObject = Instantiate(bulletProjectile, transform.position, Quaternion.identity);
            SlimBulletProjectile bulletScript = bulletObject.GetComponent<SlimBulletProjectile>();
            bulletScript.SetTarget(targetTransform);
            if (isAlly)
                bulletScript.SetToAllyBullet();
            timeBetweenShots = startTimeBEtweenShots;
        }
        else
        {
            timeBetweenShots -= Time.deltaTime;
            if (timeBetweenShots < 0)
                timeBetweenShots = 0;
        }
    }
}
