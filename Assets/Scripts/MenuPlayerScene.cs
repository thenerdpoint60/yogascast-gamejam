﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPlayerScene : Player
{
    public GameObject settingScene;

    private void Start()
    {
        
        settingScene.SetActive(false);
    }

    private void Update()
    {
        MoveUpdate();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "StartTrigger")
        {
            SceneManager.LoadScene("gameScene");
        }
        else if (collision.name == "SettingTrigger")
        {
            Debug.Log("Setting Scene");
            GetComponent<SpriteRenderer>().enabled = false;
            settingScene.SetActive(true);
            //Load the setting scene
        }
        else if (collision.name=="QuitTrigger")
        {
            Debug.Log("Quit the game");
            
        }
    }

    void BackButton()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        settingScene.SetActive(false);
    }


}
