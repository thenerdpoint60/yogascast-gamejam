﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour
{
    Rigidbody2D rigidbody;

    [SerializeField]
    GameObject attachObject;
    Player player;
    private bool bIsAttach;
    public float attachSpeed;
    public float attachTimer;
    private float attachTimerCounter;
    private bool isOnlyThePlayerPicks; // if only the player be able to pick the heart
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        player = attachObject.GetComponent<Player>(); // the first attach object will always be the player
        bIsAttach = true;
    }

    void Update()
    {
        if (bIsAttach)
        {
            Vector2 position = (Vector2)attachObject.transform.position + Vector2.down * 0.3f;
            if (attachObject.Equals(player.gameObject))
                position = (Vector2)attachObject.transform.position;
            if (attachTimerCounter > 0)
                transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * attachSpeed);
            else
                transform.position = position;
            attachTimerCounter -= 0.1f;
        }
        else
            ThrownAway();
    }

    private bool bIsThrow; // is the heart still flying away from player
    private float fHeartSpeed = 0;
    private float fHeartSpeedDec = 0.12f;
    private Vector2 vHeartThrowDir = Vector2.up;
    public void ThrowHeart()
    {
        // player throws heart when he moves
        vHeartThrowDir = player.GetMovDirection().normalized;
        bIsAttach = false;
        bIsThrow = true;
        fHeartSpeed = player.heartThrowForce;
        fHeartSpeedDec = 0.12f;
        isOnlyThePlayerPicks = false;
    }
    public void DropHeart()
    {
        // when the enemy dies, he will drop the heart, but no enemy can pick it since the player didn't give it personally
        ThrowHeart();
        vHeartThrowDir = player.transform.position.normalized;
        fHeartSpeedDec = 1f;
        isOnlyThePlayerPicks = true;
    }
    private void ThrownAway()
    {
        // thrown heart flyes from the player (the speed will decrease until it stops)
        if (fHeartSpeed <= 0)
        {
            fHeartSpeed = 0;
            bIsThrow = false;
            return;
        }
        Vector2 vThrowDir = new Vector2(vHeartThrowDir.x * fHeartSpeed, vHeartThrowDir.y * fHeartSpeed);
        rigidbody.velocity = vThrowDir;
        fHeartSpeed -= fHeartSpeedDec;
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        string tag = col.gameObject.tag;
        switch (tag)
        {
            case "Player":
                if (col.GetType().ToString() == "UnityEngine.CapsuleCollider2D")
                {
                    if (!IsThrowing())
                        AttachToNewBody(col.transform.gameObject);
                }
                break;
        }
    }

    public void AttachToNewBody(GameObject attachObject)
    {
        // attach the heart to new body. Enemies can pick only if enemies are allowed
        if (!attachObject.Equals(player.gameObject) && isOnlyThePlayerPicks)
            return;
        bIsAttach = true;
        this.attachObject = attachObject;
        attachTimerCounter = attachTimer;
    }
    public bool IsAttachedToBody(GameObject gameObject) { return gameObject.Equals(attachObject); }
    public bool IsAttachedToBody() { return bIsAttach; }
    public bool IsThrowing() { return bIsThrow; }

}
