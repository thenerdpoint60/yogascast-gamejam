﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKey : Enemy
{
    void Start()
    {
        StartEnemy();
    }

    void Update()
    {
        tag = FRIENDLY_TAG;
        UpdateEnemy();
        if (IsDead())
            return;
        Jump();
    }

    private void Jump()
    {
        Transform target = GetTarget();
        float distanceBetweenPlayer = Vector2.Distance(transform.position, playerTransform.position);
        bool isTooFarAwayFromPlayer = distanceBetweenPlayer > dangerDistance;
        bool isEnemiesFarAwayFromPlayer = !IsEnemiesNearPlayer();
        if (isAlly)
        {
                JumpTowards(playerTransform.position);
        }
        else if (GetMaximumPlayerFollowDistance() < distanceBetweenPlayer)
            JumpTowards(target.position);
    }
}
